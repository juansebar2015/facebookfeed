//
//  ViewController.swift
//  facebookFeed
//
//  Created by Juan Ramirez on 5/18/17.
//  Copyright © 2017 Juan Ramirez. All rights reserved.
//

import UIKit

let cellID = "cellID"

class Post {
    var name: String?
    var statusText: String?
    var profileImageName: String?
    var statusImageName: String?
    var numLikes: Int?
    var numComments: Int?
    var statusImageUrl: String?
}

class FeedViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout {

    var refreshControl: UIRefreshControl!
    
    var posts = [Post]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let postMark = Post()
        postMark.name = "Mark Zuckerberg"
        postMark.statusText = "Meanwhile, Beast turned to the dark side."
        postMark.profileImageName = "zuckprofile"
        //postMark.statusImageName = "zuckdog"
        postMark.statusImageUrl = "https://turntable.kagiso.io/images/mark_zuckerbergs_dog.original.png"
        postMark.numLikes = 4000313
        postMark.numComments = 123
        
        let postSteve = Post()
        postSteve.name = "Steve Jobs"
        postSteve.statusText = "Design is not just what it looks like and feels like. Design is how it works.\n\n" +
            "Being the richest man in the cemetery doesn't matter to me. Going to bed at night saying we've done something wonderful, that's what matters to me.\n\n" +
            "Sometimes when you innovate, you make mistakes. It is best to admit them quickly, and get on with improving your other innovations."
        postSteve.profileImageName = "steve_profile"
        //postSteve.statusImageName = "steve_status"
        postSteve.statusImageUrl = "http://deskcomputers.info/wp-content/uploads/2016/06/steve-jobs-3.jpg"
        postSteve.numLikes = 1000
        postSteve.numComments = 5500000000
        
        let postGhandi = Post()
        postGhandi.name = "Gandhi"
        postGhandi.statusText = "Live as if you were to die tomorrow; learn as if you were to live forever.\n" +
            "The weak can never forgive. Forgiveness is the attribute of the strong.\n" +
            "Happiness is when what you think, what you say, and what you do are in harmony."
        postGhandi.profileImageName = "gandhi"
        //postGhandi.statusImageName = "gandhi_status"
        postGhandi.statusImageUrl = "http://pressks.com/wp-content/uploads/2016/10/Gandhi-Jayanti-Inspirational-Quotes-Wishes-Whatsapp-Status-Facebook-Messages.jpg"
        postGhandi.numLikes = 633
        postGhandi.numComments = 155
        
        posts.append(postMark)
        posts.append(postSteve)
        posts.append(postGhandi)
        

        refreshControl = UIRefreshControl()
        refreshControl.tintColor = UIColor.rgb(red: 70, green: 146, blue: 250, alpha: 1)
        refreshControl.addTarget(self, action: #selector(FeedViewController.loadData), for: .valueChanged)
        collectionView?.refreshControl = refreshControl
        
        collectionView?.alwaysBounceVertical = true     // Make collection View bounce when dragged
        
        collectionView?.backgroundColor = UIColor(white: 0.95, alpha: 1)
        
        collectionView?.register(FeedCell.self, forCellWithReuseIdentifier: cellID)
    }

    func loadData() {
        _ = Timer.scheduledTimer(withTimeInterval: 3.0, repeats: false) { (timer) in
            
            // Reload CollecitonView
        
            self.refreshControl.endRefreshing()
        }
        
        
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return posts.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let feedCell = collectionView.dequeueReusableCell(withReuseIdentifier: cellID, for: indexPath) as! FeedCell
        
        feedCell.post = posts[indexPath.item]
        
        //Set the Controller
        feedCell.feedController = self
        
        return feedCell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        //Estimate the size for the cell
        if let statusText = posts[indexPath.item].statusText {
            
            //Calculate Height of textView
                                                    // Estimates text size
            let rect = NSString(string: statusText).boundingRect(with: CGSize.init(width: view.frame.width, height: 1000), options: NSStringDrawingOptions.usesFontLeading.union(NSStringDrawingOptions.usesLineFragmentOrigin), attributes: [NSFontAttributeName : UIFont.systemFont(ofSize: 14)], context: nil)
            
            // Must calculate the height of all with the custom textView
            let knownHeight: CGFloat = 8 + 44 + 4 + 4 + 200 + 8 + 24 + 8 + 44
            
            return CGSize(width: view.frame.width, height: rect.height + knownHeight + 24)  // 24 is extra buffer space
            
            /*
             * LEFT AT MIN 15:43
 
             */
        }
        
        return CGSize(width: view.frame.width, height: 500)
    }
    
    
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        
        // If device changes orientation, invalidate the current layout and re-draw yourself
        collectionView?.collectionViewLayout.invalidateLayout()
    }
    
    let blackBacgroundView = UIView()
    let zoomImageView = UIImageView()
    let navBarCoverView = UIView()
    let tabBarCoverView = UIView()
    
    var statusImageView: UIImageView?
    
    func animateImageView(statusImageView: UIImageView) {
        
        self.statusImageView = statusImageView
        
        // How you get the absolute position of the view
        if let startingFrame = statusImageView.superview?.convert(statusImageView.frame, to: nil) {
            
            statusImageView.alpha = 0
            
            blackBacgroundView.frame = view.frame
            blackBacgroundView.backgroundColor = .black
            blackBacgroundView.alpha = 0
            view.addSubview(blackBacgroundView)
            
            navBarCoverView.frame = CGRect(x: 0, y: 0, width: 10000, height: 20 + 44)
            navBarCoverView.backgroundColor = .black
            navBarCoverView.alpha = 0

            
            if let keyWindow = UIApplication.shared.keyWindow {
                keyWindow.addSubview(navBarCoverView)
                
                tabBarCoverView.frame = CGRect(x: 0, y: keyWindow.frame.height - 49, width: 1000, height: 49)
                tabBarCoverView.backgroundColor = .black
                tabBarCoverView.alpha = 0
                
                keyWindow.addSubview(tabBarCoverView)
            }
            
            zoomImageView.frame = startingFrame
            zoomImageView.isUserInteractionEnabled = true
            zoomImageView.image = statusImageView.image
            zoomImageView.contentMode = .scaleAspectFill
            zoomImageView.layer.masksToBounds = true
            
            // Add tap gesture recognizer
            zoomImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(FeedViewController.zoomOut) ))
            
            view.addSubview(zoomImageView)
            
            UIView.animate(withDuration: 0.75, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 0.5, options: .curveEaseOut, animations: { 
                let newY = (self.view.frame.height / 2) - (startingFrame.height / 2)
                
                self.zoomImageView.frame = CGRect(x: 0, y: newY, width: startingFrame.width, height: startingFrame.height)
                
                self.blackBacgroundView.alpha = 1
                self.navBarCoverView.alpha = 1
                self.tabBarCoverView.alpha = 1
            }, completion: nil)
        }
    }
    
    func zoomOut() {
        
        if let startingFrame = statusImageView?.superview?.convert(statusImageView!.frame, to: nil) {
            
            UIView.animate(withDuration: 0.75, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 0.5, options: .curveEaseOut, animations: { 
                self.zoomImageView.frame = startingFrame
                
                self.blackBacgroundView.alpha = 0
                self.navBarCoverView.alpha = 0
                self.tabBarCoverView.alpha = 0
            }, completion: { (didComplete) in
                self.zoomImageView.removeFromSuperview()
                self.blackBacgroundView.removeFromSuperview()
                self.navBarCoverView.removeFromSuperview()
                self.tabBarCoverView.removeFromSuperview()
                
                self.statusImageView?.alpha = 1
            })
        }
        
    }
    
}

var imageCache = NSCache<AnyObject, AnyObject>() //[String: UIImage]()


class FeedCell : UICollectionViewCell {
    
    var feedController : FeedViewController?
    
    func animateImage() {
        // Give acces to FeedViewController to statusImageView
        feedController?.animateImageView(statusImageView: statusImageView)
    }
    
    var post : Post? {
        didSet{
            if let name = post?.name {
                let attributedText = NSMutableAttributedString(string: name, attributes: [NSFontAttributeName : UIFont.boldSystemFont(ofSize: 14)])
                
                attributedText.append(NSAttributedString(string: "\nDecember 18  •  San Francisco  •  ", attributes: [NSFontAttributeName : UIFont.systemFont(ofSize: 12), NSForegroundColorAttributeName: UIColor.rgb(red: 155, green: 161, blue: 171, alpha: 1)]))
                
                
                //Increase the line spacing
                let paragraphStyle = NSMutableParagraphStyle()
                paragraphStyle.lineSpacing = 4
                
                attributedText.addAttribute(NSParagraphStyleAttributeName, value: paragraphStyle, range: NSMakeRange(0, attributedText.string.characters.count))
                
                // Add image to attributed Text
                let attachment = NSTextAttachment()
                attachment.image = #imageLiteral(resourceName: "globe_small")
                attachment.bounds = CGRect(x: 0, y: -3, width: 12, height: 12)  // x & y modifies location for attachemnt in the string
                attributedText.append(NSAttributedString(attachment: attachment))
                
                nameLabel.attributedText = attributedText
            }
            
            if let statusText = post?.statusText {
                statusTextView.text = statusText
            }
            
            if let profileImageName = post?.profileImageName {
                profileImageView.image = UIImage(named: profileImageName)
            }
            
            if let statusImageName = post?.statusImageName {
                statusImageView.image = UIImage(named: statusImageName)
            }
            
            if let numOfLikes = post?.numLikes , let numOfComments = post?.numComments {
                
                let likes = self.checkQuanity(num: numOfLikes)
                let comments = self.checkQuanity(num: numOfComments)
                
                likesCommentLabel.text = "\(likes) Likes   \(comments) Comments"
            }
            
            
            if let statusImageUrl = post?.statusImageUrl {
                
                if let image = imageCache.object(forKey: statusImageUrl as NSString) as? UIImage{
                    DispatchQueue.main.async {
                        self.statusImageView.image = image
                        self.loader.stopAnimating()
                    }
                } else {
                    URLSession.shared.dataTask(with: URL(string: statusImageUrl)!, completionHandler: { (data, response, error) in
                        
                        if error != nil {
                            print(error!)
                            return
                        }
                        
                        let image = UIImage(data: data!)
                        
                        imageCache.setObject(image!, forKey: statusImageUrl as NSString)
                        
                        DispatchQueue.main.async {  // Update image on main thread
                            self.statusImageView.image = image
                            self.loader.stopAnimating()
                        }
                        
                    }).resume()
                }
            }
        }
    }
    
    func checkQuanity(num: Int) -> String {
        let billion = 1000000000
        let million = 1000000
        let thousand = 1000
        
        var result : String!
        
        if num >= billion {
            result = "\(Float(num / billion))B"
        } else if num >= million {
            result = "\(Float(num / million))M"
        } else if num >= thousand {
            result = "\(Float(num / thousand))K"
        } else {
            result = "\(num)"
        }
        
        return result
    }
    
    // Called when cell is dequed
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    let nameLabel : UILabel = {
        let label = UILabel()
        label.numberOfLines = 2
        
        return label
    }()
    
    let profileImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        //imageView.image = #imageLiteral(resourceName: "zuckprofile")
        imageView.clipsToBounds = true
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    let statusTextView : UITextView = {
        let textView = UITextView()
        textView.text = "Meanwhile, Beast turned to the dark side."
        textView.font = UIFont.systemFont(ofSize: 14)
        textView.isScrollEnabled = false
        textView.isEditable = false
        return textView
    }()
    
    let statusImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        //imageView.image = #imageLiteral(resourceName: "zuckdog")
        imageView.layer.masksToBounds = true
        imageView.isUserInteractionEnabled = true
        return imageView
    }()
    
    let likesCommentLabel : UILabel = {
        let label = UILabel()
        label.text = "488 Likes   10.7K Comments"
        label.font = UIFont.systemFont(ofSize: 12)
        label.textColor = UIColor.rgb(red: 155, green: 161, blue: 171, alpha: 1)   //UIColor.init(red: 155/255, green: 161/255, blue: 171/255, alpha: 1)
        return label
    }()
    
    let dividerLineView : UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.rgb(red: 226, green: 228, blue: 232, alpha: 1)
        return view
    }()
    
    let buttonContainerView : UIView = {
        let view = UIView()
        view.backgroundColor = .white
        return view
    }()
    
    let likeButton : UIButton = FeedCell.buttonForTitle(title: "Like", imageName: #imageLiteral(resourceName: "like"))
    let commentButton : UIButton = FeedCell.buttonForTitle(title: "Comment", imageName: #imageLiteral(resourceName: "comment"))
    let shareButton : UIButton = FeedCell.buttonForTitle(title: "Share", imageName: #imageLiteral(resourceName: "share"))
    
    // Returns the buttons for action on a feed post
    static func buttonForTitle(title: String, imageName: UIImage) -> UIButton {
        let button = UIButton()
        button.setTitle(title, for: .normal)
        button.setTitleColor(UIColor.rgb(red: 143, green: 150, blue: 163, alpha: 1), for: .normal)
        
        button.setImage(imageName, for: .normal)
        button.titleEdgeInsets = UIEdgeInsetsMake(0, 8, 0, 0)  // (top, left, bottom, right)
        
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        return button
    }
    
    var loader : UIActivityIndicatorView = {
        let activityIndicatorView = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
        activityIndicatorView.color = UIColor.black
        return activityIndicatorView
    }()

    
    func setupViews(){
        backgroundColor = .white
        
        addSubview(nameLabel)
        addSubview(profileImageView)
        addSubview(statusTextView)
        addSubview(statusImageView)
        addSubview(likesCommentLabel)
        addSubview(dividerLineView)
        
        addSubview(buttonContainerView)
        
        statusImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(FeedCell.animateImage)))
        
        buttonContainerView.addSubview(likeButton)
        buttonContainerView.addSubview(commentButton)
        buttonContainerView.addSubview(shareButton)
        
        // Horizontal Constraints
        addConstraintsWithFormat(format: "H:|-8-[v0(44)]-8-[v1]|", views: profileImageView, nameLabel)
        
        addConstraintsWithFormat(format: "H:|-4-[v0]-4-|", views: statusTextView)
        
        addConstraintsWithFormat(format: "H:|[v0]|", views: statusImageView)
        
        addConstraintsWithFormat(format: "H:|-12-[v0]|", views: likesCommentLabel)
        
        addConstraintsWithFormat(format: "H:|-12-[v0]-12-|", views: dividerLineView)
        
        addConstraintsWithFormat(format: "H:|[v0]|", views: buttonContainerView)
        
        // [v0(v2)][v1(v2)][v2] -> This makes v0, v1 have equal spacing as v2
        buttonContainerView.addConstraintsWithFormat(format: "H:|[v0(v2)][v1(v2)][v2]|", views: likeButton, commentButton, shareButton)
        
        
        //Vertical Constraints
        addConstraintsWithFormat(format: "V:|-12-[v0]", views: nameLabel)
        
        addConstraintsWithFormat(format: "V:|-8-[v0(44)]-4-[v1]-4-[v2(200)]-8-[v3(24)]-8-[v4(0.5)][v5(44)]|", views: profileImageView, statusTextView, statusImageView, likesCommentLabel, dividerLineView, buttonContainerView)
        
        buttonContainerView.addConstraintsWithFormat(format: "V:|[v0]|", views: likeButton)
        buttonContainerView.addConstraintsWithFormat(format: "V:|[v0]|", views: commentButton)
        buttonContainerView.addConstraintsWithFormat(format: "V:|[v0]|", views: shareButton)

        
        statusImageView.addSubview(loader)
        
        statusImageView.addConstraintsWithFormat(format: "H:|[v0]|", views: loader)
        statusImageView.addConstraintsWithFormat(format: "V:|[v0]|", views: loader)
        
        loader.startAnimating()
    }
    
   }

extension UIColor {
    static func rgb(red: CGFloat, green: CGFloat, blue: CGFloat, alpha: CGFloat) -> UIColor {
        return UIColor.init(red: red/255, green: green/255, blue: blue/255, alpha: alpha)
    }
}

extension UIView {
    func addConstraintsWithFormat(format: String, views: UIView...) {
        var viewsDictionary = [String: UIView]()
        
        for(index, view) in views.enumerated() {
            let key = "v\(index)"
            viewsDictionary[key] = view
            view.translatesAutoresizingMaskIntoConstraints = false
        }
        
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: format, options: NSLayoutFormatOptions(), metrics: nil, views: viewsDictionary))
    }
}
