//
//  CustomTabBarController.swift
//  facebookFeed
//
//  Created by Juan Ramirez on 5/20/17.
//  Copyright © 2017 Juan Ramirez. All rights reserved.
//

import UIKit

class CustomTabBarController : UITabBarController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Add NewsFeed View Controller
        let feedController = FeedViewController(collectionViewLayout: UICollectionViewFlowLayout())
        feedController.navigationItem.title = "News Feed"
        let newsFeedNavController = UINavigationController(rootViewController: feedController)
        newsFeedNavController.title = "News Feed"
        newsFeedNavController.tabBarItem.image = #imageLiteral(resourceName: "news_feed_icon")
        
        // Add FriendRequestViewController
        let friendsRequestController = FriendRequestController()
        let friendsRequestNavController = UINavigationController(rootViewController: friendsRequestController)
        friendsRequestNavController.title = "Requests"
        friendsRequestNavController.tabBarItem.image = #imageLiteral(resourceName: "requests_icon")
        
        // Add Messenger View Controller
        let messengerController = UITableViewController()
        messengerController.navigationItem.title = "Messages"
        let messengerNavigationController = UINavigationController(rootViewController: messengerController)
        messengerNavigationController.title = "Messenger"
        messengerNavigationController.tabBarItem.image = #imageLiteral(resourceName: "messenger_icon")
        
        // Add Notification View Controller
        let notificationController = UITableViewController()
        notificationController.navigationItem.title = "Notifications"
        let notificationNavController = UINavigationController(rootViewController: notificationController)
        notificationNavController.title = "Notifications"
        notificationNavController.tabBarItem.image = #imageLiteral(resourceName: "globe_icon")
        
        // Add More View Controller
        let moreViewController = UITableViewController()
        moreViewController.navigationItem.title = "More"
        let moreNavController = UINavigationController(rootViewController: moreViewController)
        moreNavController.title = "More"
        moreNavController.tabBarItem.image = #imageLiteral(resourceName: "more_icon")
        
        // Make an array of the View Controllers
        viewControllers = [newsFeedNavController, friendsRequestNavController, messengerNavigationController, notificationNavController, moreNavController]
        
        // Remove translucensy of tab bar
        tabBar.isTranslucent = false
        
        // Create a new border
        let topBorder = CALayer()
        topBorder.frame = CGRect(x: 0, y: 0, width: 1000, height: 0.5)
        topBorder.backgroundColor = UIColor.rgb(red: 229, green: 231, blue: 235, alpha: 1).cgColor
        
        tabBar.layer.addSublayer(topBorder)
        
        // Removes the current divider line
        tabBar.clipsToBounds = true
        
        // Modify the tint color of the Tab Bar
        tabBar.tintColor = UIColor.rgb(red: 70, green: 146, blue: 250, alpha: 1)
        tabBar.unselectedItemTintColor = UIColor.lightGray
    }
}
